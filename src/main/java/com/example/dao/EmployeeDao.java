package com.example.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.Employee;
import com.example.model.EmployeeMapper;
import com.example.model.EmployeeRoleMapper;

import java.util.*;

@Transactional
@Repository

public class EmployeeDao {

	@Autowired
	private JdbcTemplate jdbc;
	
	public List<Employee> getAll(){
		
		String sql = "select * from employee order by employeeId asc";
		List<Employee> ep = jdbc.query(sql, new EmployeeMapper());
		
		return ep; 		
	}
	
	public Employee getById(int id) {
		
		String sql = "select * from employee where employeeId = ?";
		Employee ep = jdbc.queryForObject(sql, new Object[] {id}, new EmployeeMapper());
		
		return ep;				
	}
	
	public void addEmployee(Employee ep) {
		
		String sql = "insert into employee (employeeroleid,employeename,phone,address) VALUES (?,?,?,?)";
		
		jdbc.update(sql, ep.getEmployeeRoleId(), ep.getEmployeeName(), ep.getEmployeePhone(), ep.getEmployeeAddress()  );
	}
	
	public void updateEmployee(Employee ep, int id) {
		
		String sql = "update employee set employeeroleid=?, employeename=?, phone=?, address=? where employeeid=?";
		
		jdbc.update(sql, ep.getEmployeeRoleId(), ep.getEmployeeName(), ep.getEmployeePhone(), ep.getEmployeeAddress(),id);
	}
	
	public void deleteEmployeeById(int id) {
		
		String sql = "delete from employee where employeeid=?";
		jdbc.update(sql,id);		
	}
	
	public int latestInput() {//To get latest ID number
		String sql = "SELECT currval(pg_get_serial_sequence('employee','employeeid'))";
		
		int id = jdbc.queryForObject(sql, Integer.class);
		
		return id;
	}
	
	
	
}
