package com.example.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import com.example.model.EmployeeRole;
import com.example.model.EmployeeRoleMapper;

@Transactional
@Repository
public class EmployeeRoleDao {
	
	@Autowired
	private JdbcTemplate jdbc;
	
		
	public List<EmployeeRole> getAll(){
		
		String sql = "select * from employeeRole order by employeeroleid asc";
		List<EmployeeRole> er = jdbc.query(sql, new EmployeeRoleMapper());
		
		return er; 		
	}
	
	public EmployeeRole getById(int id) {
		
		String sql = "select * from employeerole where employeeroleid = ?";
		EmployeeRole er = jdbc.queryForObject(sql, new Object[] {id}, new EmployeeRoleMapper());
		
		return er;				
	}
	
	public void addEmployeeRole(EmployeeRole er) {
		
		String sql = "INSERT INTO employeerole (employeerolename) VALUES (?)";
		
		jdbc.update(sql, er.getEmployeeRoleName());
	}
	
	public void updateEmployeeRole(EmployeeRole er, int id) {
		
		String sql = "update employeerole set employeerolename=? where employeeroleid=?";
		
		jdbc.update(sql,er.getEmployeeRoleName(),id);
	}
	
	public void deleteEmployeeRoleById(int id) {
		
		String sql = "delete from employeerole where employeeroleid=?";
		jdbc.update(sql,id);		
	}

	public int latestInput() {//To get latest ID number
		String sql = "SELECT currval(pg_get_serial_sequence('employeerole','employeeroleid'))";
		
		int id = jdbc.queryForObject(sql, Integer.class);
		
		return id;
	}
	
}
