package com.example.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EmployeeMapper implements RowMapper<Employee> {

	@Override
	public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		
		Employee e = new Employee();
		e.setEmployeeId(rs.getInt("employeeid"));
		e.setEmployeeRoleId(rs.getInt("employeeroleid"));
		e.setEmployeeName(rs.getString("employeename"));
		e.setEmployeePhone(rs.getString("Phone"));
		e.setEmployeeAddress(rs.getString("Address"));
		return e;
		
	}

}
