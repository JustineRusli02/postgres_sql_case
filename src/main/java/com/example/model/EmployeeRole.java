package com.example.model;

public class EmployeeRole {
	
	private int employeeRoleId;
	private String employeeRoleName;
	
	public int getEmployeeRoleId() {
		return employeeRoleId;
	}
	public void setEmployeeRoleId(int employeeRoleId) {
		this.employeeRoleId = employeeRoleId;
	}
	public String getEmployeeRoleName() {
		return employeeRoleName;
	}
	public void setEmployeeRoleName(String employeeRoleName) {
		this.employeeRoleName = employeeRoleName;
	}
	
	

}
