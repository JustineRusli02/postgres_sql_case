package com.example.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EmployeeRoleMapper implements RowMapper<EmployeeRole> {

	@Override
	public EmployeeRole mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		EmployeeRole er = new EmployeeRole();
		er.setEmployeeRoleId(rs.getInt("employeeroleid"));
		er.setEmployeeRoleName(rs.getString("employeerolename"));
		
		return er;
	}

}
