package com.example.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.EmployeeRoleDao;
import com.example.model.EmployeeRole;

@Service
public class EmployeeRoleService {

	@Autowired
	EmployeeRoleDao ers;

	public List<EmployeeRole> getAll() {

		return ers.getAll();

	}

	public EmployeeRole getById(int id) {

		return ers.getById(id);
	}

	public void add(EmployeeRole employeeRole) {

		ers.addEmployeeRole(employeeRole);

	}

	public void update(EmployeeRole employeeRole, int id) {
		ers.updateEmployeeRole(employeeRole, id);

	}

	public void deleteById(int id) {
		ers.deleteEmployeeRoleById(id);

	}
	
	public int latestInput() {
		
		return ers.latestInput();
	}

}
