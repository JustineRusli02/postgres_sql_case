package com.example.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.EmployeeDao;
import com.example.model.Employee;

@Service
public class EmployeeService {

	@Autowired
	EmployeeDao ed;
	
	
	public List<Employee> getAll() {
		
		
		return ed.getAll();
		
		
	}

	
	public Employee getById(int id) {
		
		return ed.getById(id);
	}

	
	public void add(Employee employee) {
		
		ed.addEmployee(employee);
		
	}

	
	public void update(Employee employee, int id) {
		ed.updateEmployee(employee, id);
		
	}

	
	public void deleteById(int id) {
		ed.deleteEmployeeById(id);
		
	}
	
	public int latestInput() {
		
		return ed.latestInput();
	}

}
