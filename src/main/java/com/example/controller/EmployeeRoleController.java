package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


import com.example.model.EmployeeRole;
import com.example.services.EmployeeRoleService;



@RequestMapping("employeeRole")
@Controller
public class EmployeeRoleController {

	
	@Autowired
	EmployeeRoleService ers;
	
	@GetMapping("/{id}")
	public ResponseEntity<EmployeeRole> getById(@PathVariable("id") int id) {

		EmployeeRole er = ers.getById(id);
		return new ResponseEntity<EmployeeRole>(er, HttpStatus.OK);

	}

	@GetMapping("/")
	public ResponseEntity<List<EmployeeRole>> getAll() {
		List<EmployeeRole> listEmployeeRole = ers.getAll();
		return new ResponseEntity<List<EmployeeRole>>(listEmployeeRole, HttpStatus.OK);
	}

	@PostMapping("/")
	public ResponseEntity<EmployeeRole> addEmployee(@RequestBody EmployeeRole employeeRole) {

		ers.add(employeeRole);
		EmployeeRole er = ers.getById(ers.latestInput());

		return new ResponseEntity<EmployeeRole>(er, HttpStatus.OK);

	}

	@PutMapping("/{id}")
	public ResponseEntity<EmployeeRole> updateEmployee(@PathVariable("id") int id, @RequestBody EmployeeRole employeeRole) {

		ers.update(employeeRole, id);
		EmployeeRole er = ers.getById(id);

		return new ResponseEntity<EmployeeRole>(er, HttpStatus.OK);

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<EmployeeRole> deleteEmployee(@PathVariable("id") int id) {

		EmployeeRole er = ers.getById(id);
		ers.deleteById(id);

		return new ResponseEntity<EmployeeRole>(er, HttpStatus.OK);

	}
}
