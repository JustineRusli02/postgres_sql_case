package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.model.Employee;
import com.example.services.EmployeeService;

@RequestMapping("employee")
@Controller
public class EmployeeController {

	@Autowired
	EmployeeService es;
	
	@GetMapping("/{id}")
	public ResponseEntity<Employee> getById(@PathVariable("id") int id) {

		Employee e = es.getById(id);
		return new ResponseEntity<Employee>(e, HttpStatus.OK);

	}

	@GetMapping("/")
	public ResponseEntity<List<Employee>> getAll() {
		List<Employee> listEmployee = es.getAll();
		return new ResponseEntity<List<Employee>>(listEmployee, HttpStatus.OK);
	}

	@PostMapping("/")
	public ResponseEntity<Employee> addEmployee(@RequestBody Employee employee) {

		es.add(employee);
		Employee ep = es.getById(es.latestInput());

		return new ResponseEntity<Employee>(ep, HttpStatus.OK);

	}

	@PutMapping("/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable("id") int id, @RequestBody Employee employee) {

		es.update(employee, id);
		Employee ep = es.getById(id);

		return new ResponseEntity<Employee>(ep, HttpStatus.OK);

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Employee> deleteEmployee(@PathVariable("id") int id) {

		Employee ep = es.getById(id);
		es.deleteById(id);

		return new ResponseEntity<Employee>(ep, HttpStatus.OK);

	}
	
}
